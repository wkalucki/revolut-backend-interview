package com.revolut.backend.interview.rest.handlers;

import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.model.Deposit;
import com.revolut.backend.interview.model.Transfer;
import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.persistence.TransactionService;
import com.revolut.backend.interview.validators.ValidationException;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class TransferHandlerTest {


    private TransactionHandler transactionHandler;
    private TransactionService transactionService;
    private AccountRepository accountRepository;


    @BeforeEach
    void setUp() {
        this.transactionService = mock(TransactionService.class);
        this.accountRepository = mock(AccountRepository.class);
        this.transactionHandler = new TransactionHandler(accountRepository, transactionService);
    }


    @Test
    void performTransfer_CustomersExist() throws ValidationException {
        Context context = createContext();
        String customerFrom = "customerFrom";
        String customerTo = "customerTo";
        BigDecimal amount = new BigDecimal("500");
        Transfer transfer = new Transfer(customerFrom, customerTo, amount);
        when(context.bodyAsClass(Transfer.class)).thenReturn(transfer);

        Account accountFrom = new Account(customerFrom);
        when(accountRepository.findByCustomerId(customerFrom)).thenReturn(Optional.of(accountFrom));
        Account accountTo = new Account(customerTo);
        when(accountRepository.findByCustomerId(customerTo)).thenReturn(Optional.of(accountTo));
        assertDoesNotThrow(() -> transactionHandler.performTransfer(context));
        verify(transactionService, times(1)).transfer(accountFrom, accountTo, amount);

    }

    @Test
    void performTransfer_CustomerNotExists() {
        Context context = createContext();
        String customerFrom = "customerFrom";
        String customerTo = "customerTo";
        BigDecimal amount = new BigDecimal("100");
        Transfer transfer = new Transfer(customerFrom, customerTo, amount);
        when(context.bodyAsClass(Transfer.class)).thenReturn(transfer);

        when(accountRepository.findByCustomerId(customerFrom)).thenReturn(Optional.empty());
        when(accountRepository.findByCustomerId(customerTo)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class, () -> transactionHandler.performTransfer(context));
    }


    @Test
    void deposit_CustomerExists() {
        Context context = createContext();
        String customer = "customer";
        Account account = new Account(customer);
        BigDecimal amount = new BigDecimal("100");
        Deposit deposit = new Deposit(customer, amount);
        when(context.bodyAsClass(Deposit.class)).thenReturn(deposit);

        when(accountRepository.findByCustomerId(customer)).thenReturn(Optional.of(account));
        assertDoesNotThrow(() -> transactionHandler.deposit(context));
    }

    @Test
    void deposit_CustomerNotExists() {
        Context context = createContext();
        String customer = "customer";
        BigDecimal amount = new BigDecimal("100");
        Deposit deposit = new Deposit(customer, amount);
        when(context.bodyAsClass(Deposit.class)).thenReturn(deposit);

        when(accountRepository.findByCustomerId(customer)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class, () -> transactionHandler.deposit(context));
    }

    private Context createContext() {
        return mock(Context.class);
    }
}