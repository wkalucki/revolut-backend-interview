package com.revolut.backend.interview.rest.handlers;

import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.persistence.DuplicateAccountException;
import com.revolut.backend.interview.validators.ValidationException;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AccountControllerTest {

    private AccountHandler accountHandler;
    private AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        accountRepository = mock(AccountRepository.class);
        accountHandler = new AccountHandler(accountRepository);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "     "})
    void whenCreatingAccountWithInvalidIdThenThrowException(String value) throws DuplicateAccountException {
        var context = createContextWithCustomerId(value);
        assertThrows(ValidationException.class, () -> accountHandler.createAccount(context), "Customer id is invalid");
        verify(accountRepository, times(0)).persist(any(Account.class));
    }

    @Test
    void whenCreatingAccountWithValidIdThenAccountCreationIsSuccessful() throws DuplicateAccountException {

        String validCustomer = "validCustomer";
        var context = createContextWithCustomerId(validCustomer);

        var account = assertDoesNotThrow(() -> accountHandler.createAccount(context));
        verify(accountRepository, times(1)).persist(account);
        assertEquals(account.getCustomerId(), validCustomer);
        assertEquals(account.getBalance(), BigDecimal.ZERO);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "     "})
    void whenRetrievingAccountWithInvalidIdThenThrowException(String value) throws DuplicateAccountException {
        var context = createContextWithCustomerId(value);
        assertThrows(ValidationException.class, () -> accountHandler.retrieveAccount(context), "Customer id is invalid");
        verify(accountRepository, times(0)).persist(any(Account.class));
    }

    @Test
    void whenRetrievingAccountForNonExistingCustomerThenThrowException() throws DuplicateAccountException {
        var context = createContextWithCustomerId("notExistingCustomer");
        assertThrows(NotFoundResponse.class, () -> accountHandler.retrieveAccount(context));
        verify(accountRepository, times(0)).persist(any(Account.class));
    }

    private Context createContextWithCustomerId(String customerId) {
        Context mock = mock(Context.class);
        when(mock.pathParam("customerId")).thenReturn(customerId);
        return mock;
    }
}