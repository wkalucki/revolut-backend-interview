package com.revolut.backend.interview.persistence;

import com.revolut.backend.interview.model.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.*;

class AccountRepositoryTest {

    private AccountRepository accountRepository;
    private ConcurrentHashMap<String, Account> accountPersistence;

    @BeforeEach
    void setUp() {
        this.accountPersistence = new ConcurrentHashMap<>();
        this.accountRepository = new AccountRepository(accountPersistence);
    }

    @Test
    void persist_NewAccount() {
        Account account = new Account("testCustomer1");
        Account result = assertDoesNotThrow(() -> accountRepository.persist(account));
        assertEquals(result, account);
    }

    @Test
    void persistExistingAccount() {
        String testCustomer1 = "testCustomer1";
        Account account = new Account(testCustomer1);
        accountPersistence.put(testCustomer1, account);
        assertThrows(DuplicateAccountException.class, () -> accountRepository.persist(account));
    }

    @Test
    void findById_ExistingAccount() {
        String testCustomer1 = "testCustomer1";
        Account account = new Account(testCustomer1);
        Optional<Account> expected = Optional.of(account);
        accountPersistence.put(testCustomer1, account);
        assertEquals(accountRepository.findByCustomerId(testCustomer1), expected);

    }

    @Test
    void findById_NotExistingAccount() {
        String testCustomer1 = "testCustomer1";
        assertEquals(accountRepository.findByCustomerId(testCustomer1), Optional.empty());
    }
}