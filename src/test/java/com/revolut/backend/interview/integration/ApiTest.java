package com.revolut.backend.interview.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.model.Deposit;
import com.revolut.backend.interview.model.Transfer;
import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.rest.ApiServer;
import io.javalin.Javalin;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ConcurrentHashMap;

import static java.net.http.HttpRequest.BodyPublishers.noBody;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ApiTest {

    private static final int BAD_REQUEST = 400;
    private static final int OK = 200;
    private HttpClient httpClient;
    private Javalin apiServer;
    private ConcurrentHashMap<String, Account> accountPersistence;

    @BeforeEach
    void setup() {
        accountPersistence = new ConcurrentHashMap<>();
        apiServer = new ApiServer(new AccountRepository(accountPersistence)).start();
        httpClient = HttpClient.newHttpClient();
    }

    @Test
    void whenCreatingNotExistingAccount_ResponseOk() throws IOException, InterruptedException {
        URI uri = URI.create("http://127.0.0.1:8080/api/account/testcustomer1");


        HttpRequest httpRequest = HttpRequest.newBuilder(uri).version(Version.HTTP_2).POST(noBody()).build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(response.statusCode(), 200);
    }

    @Test
    void whenRetrievingValidAccount_ResponseOk() throws IOException, InterruptedException {
        String testCustomer = "testCustomer";
        URI uri = URI.create("http://127.0.0.1:8080/api/account/" + testCustomer);

        accountPersistence.put(testCustomer, new Account(testCustomer));

        HttpRequest httpRequest = HttpRequest.newBuilder(uri).version(Version.HTTP_2).GET().build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(200, response.statusCode());
    }

    @Test
    void whenDepositingCorrectValue_ResponseOk() throws IOException, InterruptedException {
        String testCustomer = "testCustomer";
        accountPersistence.put(testCustomer, new Account(testCustomer));

        URI uri = URI.create("http://127.0.0.1:8080/api/deposit/testcustomer1");

        ObjectMapper objectMapper = new ObjectMapper();
        BigDecimal amount = new BigDecimal(300);
        Deposit deposit = new Deposit(testCustomer, amount);
        HttpRequest httpRequest = HttpRequest.newBuilder(uri).version(Version.HTTP_2).POST(ofString(objectMapper.writeValueAsString(deposit))).build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        assertEquals(200, response.statusCode());
        assertEquals(accountPersistence.get(testCustomer).getBalance(), amount);

    }

    @ParameterizedTest
    @ValueSource(strings = {"120.00012", "-1", "0"})
    void whenDepositingIncorrectValue_ResponseBadRequest(String value) throws IOException, InterruptedException {
        String testCustomer = "testCustomer";

        assertEquals(400, depositRequest(testCustomer, new BigDecimal(value)).statusCode());
        assertEquals(accountPersistence.get(testCustomer).getBalance(), BigDecimal.ZERO);
    }


    @Test
    void whenTransferringCorrectValue_ResponseOK_AndFundsTransferred() throws IOException, InterruptedException {

        HttpResponse<String> stringHttpResponse = transferRequest(new Transfer("fromCustomer", "toCustomer", new BigDecimal(500)));
        assertEquals(OK, stringHttpResponse.statusCode());
        assertEquals(accountPersistence.get("fromCustomer").getBalance(), new BigDecimal(500));
        assertEquals(accountPersistence.get("toCustomer").getBalance(), new BigDecimal(1500));

    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "-1", "200000000"})
    void whenTransferringIncorrectValue_ResponseBadRequest(String val) throws IOException, InterruptedException {
        HttpResponse<String> stringHttpResponse = transferRequest(new Transfer("fromCustomer", "toCustomer", new BigDecimal(val)));
        assertEquals(BAD_REQUEST, stringHttpResponse.statusCode());
    }


    private HttpResponse<String> transferRequest(Transfer transfer) throws IOException, InterruptedException {
        Account acc1 = new Account(transfer.getFromCustomerId());
        acc1.setBalance(new BigDecimal(1000));
        accountPersistence.put(transfer.getFromCustomerId(), acc1);
        Account acc2 = new Account(transfer.getToCustomerId());
        acc2.setBalance(new BigDecimal(1000));
        accountPersistence.put(transfer.getToCustomerId(), acc2);


        URI uri = URI.create("http://127.0.0.1:8080/api/transfer/");

        ObjectMapper objectMapper = new ObjectMapper();
        HttpRequest httpRequest = HttpRequest.newBuilder(uri).version(Version.HTTP_2).POST(ofString(objectMapper.writeValueAsString(transfer))).build();
        return httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

    }

    private HttpResponse<String> depositRequest(String customerId, BigDecimal amount) throws IOException, InterruptedException {
        accountPersistence.put(customerId, new Account(customerId));

        URI uri = URI.create("http://127.0.0.1:8080/api/deposit/testcustomer1");

        ObjectMapper objectMapper = new ObjectMapper();
        Deposit deposit = new Deposit(customerId, amount);
        HttpRequest httpRequest = HttpRequest.newBuilder(uri).version(Version.HTTP_2).POST(ofString(objectMapper.writeValueAsString(deposit))).build();
        return httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

    }


    @AfterEach
    void stop() {
        apiServer.stop();
    }
}
