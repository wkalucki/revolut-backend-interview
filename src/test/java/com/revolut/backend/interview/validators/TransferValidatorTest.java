package com.revolut.backend.interview.validators;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransferValidatorTest {

    private TransactionValidator transactionValidator;

    private static Stream<Arguments> notSufficientFoundsTransactions() {
        return Stream.of(
                Arguments.of(new BigDecimal("500"), new BigDecimal("100"), new BigDecimal("600")),
                Arguments.of(new BigDecimal("50"), new BigDecimal("100"), new BigDecimal("100"))
        );
    }

    private static Stream<Arguments> invalidScaleTransfer() {
        return Stream.of(
                Arguments.of(new BigDecimal("500.00001"), new BigDecimal("100"), new BigDecimal("600")),
                Arguments.of(new BigDecimal("500.01"), new BigDecimal("100.32344"), new BigDecimal("600")),
                Arguments.of(new BigDecimal("500.01"), new BigDecimal("100.000"), new BigDecimal("600.231231"))
        );
    }

    @BeforeEach
    void setUp() {
        this.transactionValidator = new TransactionValidator();
    }


    @Test
    void deposit_NullAmount() {
        assertThrows(ValidationException.class, () -> transactionValidator.validateDeposit(null));
    }

    @ParameterizedTest
    @ValueSource(strings = {"344.001", "500.2345"})
    void deposit_InvalidScale(String value) {
        assertThrows(ValidationException.class, () -> transactionValidator.validateDeposit(new BigDecimal(value)));
    }

    @ParameterizedTest
    @ValueSource(strings = {"344.11", "500.00000000"})
    void deposit_ValidScale(String value) {
        assertDoesNotThrow(() -> transactionValidator.validateDeposit(new BigDecimal(value)));
    }

    @ParameterizedTest
    @ValueSource(strings = {"0.00", "-2.00"})
    void deposit_ZeroOrNegative(String value) {
        assertThrows(ValidationException.class, () -> transactionValidator.validateDeposit(new BigDecimal(value)));

    }


    @ParameterizedTest
    @MethodSource("invalidScaleTransfer")
    void transfer_InvalidScale(BigDecimal fromBalance, BigDecimal toBalance, BigDecimal amount) {
        assertThrows(ValidationException.class, () -> transactionValidator.validateTransfer(fromBalance, toBalance, amount));
    }


    @ParameterizedTest
    @ValueSource(strings = {"344.11", "500.00000000"})
    void transfer_ValidScale(String value) {
        assertDoesNotThrow(() -> transactionValidator.validateTransfer(new BigDecimal(value), new BigDecimal(value), new BigDecimal(value)));
    }

    @ParameterizedTest
    @MethodSource("notSufficientFoundsTransactions")
    void transfer_notSufficientFounds(BigDecimal fromBalance, BigDecimal toBalance, BigDecimal amount) {
        assertThrows(ValidationException.class, () -> transactionValidator.validateTransfer(fromBalance, toBalance, amount));
    }

    @ParameterizedTest
    @ValueSource(strings = {"0.00", "-2.00"})
    void transfer_ZeroOrNegativeAmount(String value) {
        assertThrows(ValidationException.class, () -> transactionValidator.validateTransfer(new BigDecimal("500"), new BigDecimal("500"), new BigDecimal(value)));
    }

}