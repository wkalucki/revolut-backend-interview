package com.revolut.backend.interview.load;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.model.Deposit;
import com.revolut.backend.interview.model.Transfer;
import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.rest.ApiServer;
import io.javalin.Javalin;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LoadTest {

    private Javalin apiServer;
    private ConcurrentHashMap<String, Account> accountPersistence;

    @BeforeEach
    void setup() {
        accountPersistence = new ConcurrentHashMap<>();
        apiServer = new ApiServer(new AccountRepository(accountPersistence)).start();
    }

    @Test
    void loadTest_withSynchronizationValidation() throws InterruptedException, ExecutionException {
        Account customer1 = new Account("customer1");
        accountPersistence.put("customer1", customer1);
        Account customer2 = new Account("customer2");
        accountPersistence.put("customer2", customer2);

        BigDecimal balance = new BigDecimal(100000);
        customer1.setBalance(balance);
        customer2.setBalance(balance);

        ExecutorService executorService = Executors.newFixedThreadPool(16);
        var tasks1 = getAllTasks(customer1, customer2, new BigDecimal(500));
        var futures = executorService.invokeAll(tasks1);
        List<Long> times = new ArrayList<>();
        for (Future f : futures) {
            times.add((Long) f.get());
        }
        var averageTime = times.stream().collect(Collectors.averagingLong(Long::longValue));
        assertEquals(new BigDecimal(75000), customer1.getBalance());
        assertEquals(new BigDecimal(150000), customer2.getBalance());
        System.out.println("Average request time in milliseconds " + averageTime);

    }

    private List<Callable<Long>> getAllTasks(Account from, Account to, BigDecimal amount) {
        List<Callable<Long>> tasks = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            tasks.add(getTransferTask(from, to, amount));
            if (i % 2 == 0) {
                tasks.add(getTransferTask(to, from, amount));
                tasks.add(getDepositTask(to, amount));
            }
        }
        return tasks;
    }

    private Callable<Long> getTransferTask(Account from, Account to, BigDecimal amount) {
        return () -> {
            Instant start = Instant.now();
            try {

                HttpRequest httpRequest = transferRequest(new Transfer(from.getCustomerId(), to.getCustomerId(), amount));
                HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
            Instant finish = Instant.now();
            return Duration.between(start, finish).toMillis();
        };
    }

    private Callable<Long> getDepositTask(Account account, BigDecimal amount) {
        return () -> {
            Instant start = Instant.now();
            try {

                HttpRequest httpRequest = depositRequest(new Deposit(account.getCustomerId(), amount));
                HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
            Instant finish = Instant.now();
            return Duration.between(start, finish).toMillis();
        };
    }

    private HttpRequest depositRequest(Deposit deposit) throws JsonProcessingException {
        URI uri = URI.create("http://127.0.0.1:8080/api/deposit/" + deposit.getCustomerId());

        ObjectMapper objectMapper = new ObjectMapper();
        return HttpRequest.newBuilder(uri).version(HttpClient.Version.HTTP_2).POST(ofString(objectMapper.writeValueAsString(deposit))).build();

    }

    private HttpRequest transferRequest(Transfer transfer) throws JsonProcessingException {
        URI uri = URI.create("http://127.0.0.1:8080/api/transfer/");

        ObjectMapper objectMapper = new ObjectMapper();
        return HttpRequest.newBuilder(uri).version(HttpClient.Version.HTTP_2).POST(ofString(objectMapper.writeValueAsString(transfer))).build();
    }

    @AfterEach
    void stop() {
        apiServer.stop();
    }
}
