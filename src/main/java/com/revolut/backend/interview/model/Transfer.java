package com.revolut.backend.interview.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Transfer {

    private final String fromCustomerId;
    private final String toCustomerId;
    private final BigDecimal amount;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public Transfer(@JsonProperty("fromCustomerId") String fromCustomerId, @JsonProperty("toCustomerId") String toCustomerId,
                    @JsonProperty("amount") BigDecimal amount) {
        this.fromCustomerId = fromCustomerId;
        this.toCustomerId = toCustomerId;
        this.amount = amount;
    }

    public String getFromCustomerId() {
        return fromCustomerId;
    }

    public String getToCustomerId() {
        return toCustomerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
