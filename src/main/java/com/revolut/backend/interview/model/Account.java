package com.revolut.backend.interview.model;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

public class Account {

    private final String customerId;
    private final ReentrantLock lock;
    private BigDecimal balance;

    public Account(String customerId) {
        this(customerId, new BigDecimal(0), new ReentrantLock());
    }

    private Account(String customerId, BigDecimal balance, ReentrantLock lock) {
        this.customerId = customerId;
        this.balance = balance;
        this.lock = lock;
    }

    public String getCustomerId() {
        return customerId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "customerId='" + customerId + '\'' +
                ", balance=" + balance +
                '}';
    }

    public void lockAccount() {
        this.lock.lock();
    }

    public void unlockAccount() {
        this.lock.unlock();
    }
}