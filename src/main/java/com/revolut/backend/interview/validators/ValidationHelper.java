package com.revolut.backend.interview.validators;

import java.math.BigDecimal;
import java.util.Objects;

public class ValidationHelper {

    private static final Validation<BigDecimal> notNullBigDecimal = new ValidationRule<>(bigDecimal -> !Objects.isNull(bigDecimal), "Provided string cannot be null");

    private static final Validation<BigDecimal> validScale = new ValidationRule<>((bigDecimal -> bigDecimal.stripTrailingZeros().scale() <= 2), "Cash transfer can have scale of maximum 2");

    static final Validation<BigDecimal> notNullValidScale = notNullBigDecimal.and(validScale);

    private static final Validation<String> noTNullString = new ValidationRule<>(str -> !Objects.isNull(str), "Provided string cannot be null");

    private static final Validation<String> nonEmptyString = new ValidationRule<>(str -> !str.trim().isEmpty(), "Provided string cannot be empty");

    public static final Validation<String> notNullNonEmptyString = noTNullString.and(nonEmptyString);


    static Validation<BigDecimal> moreThan(BigDecimal min) {
        return new ValidationRule<>((value) -> value.compareTo(min) > 0, String.format("Value has to be more than %f", min));

    }

    static Validation<BigDecimal> moreOrEqualThan(BigDecimal min) {
        return new ValidationRule<>((value) -> value.compareTo(min) >= 0, String.format("Value has to be more or equal  %f", min));

    }
}
