package com.revolut.backend.interview.validators;

import java.math.BigDecimal;

import static com.revolut.backend.interview.validators.ValidationHelper.moreOrEqualThan;
import static com.revolut.backend.interview.validators.ValidationHelper.moreThan;
import static com.revolut.backend.interview.validators.ValidationHelper.notNullValidScale;

public class TransactionValidator {

    private static final String INVALID_AMOUNT = "Cannot transfer negative or zero amount";
    private static final String NO_SUFFICIENT_FUNDS = "No sufficient funds to perform a transfer";

    public void validateDeposit(BigDecimal depositAmount) throws ValidationException {
        validateCashField(depositAmount);
        validateAmountNotZero(depositAmount);
    }

    private void validateAmountNotZero(BigDecimal amount) throws ValidationException {
        moreThan(BigDecimal.ZERO).test(amount).onInvalidThrow(INVALID_AMOUNT);
    }

    public void validateTransfer(BigDecimal balanceFrom, BigDecimal balanceTo, BigDecimal amount) throws ValidationException {
        validateCashFields(balanceFrom, balanceTo, amount);
        validateAmountNotZero(amount);
        moreOrEqualThan(amount).test(balanceFrom).onInvalidThrow(NO_SUFFICIENT_FUNDS);
    }

    private void validateCashFields(BigDecimal... fields) throws ValidationException {
        for (BigDecimal field : fields) {
            validateCashField(field);
        }
    }

    private void validateCashField(BigDecimal amount) throws ValidationException {
        notNullValidScale.test(amount).onInvalidThrow();
    }

}
