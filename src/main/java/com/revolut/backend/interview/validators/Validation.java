package com.revolut.backend.interview.validators;

public interface Validation<T> {

    ValidationResult test(T value);

    default Validation<T> and(Validation<T> other) {
        return value -> {
            ValidationResult first = this.test(value);
            return !first.isValid() ? first : other.test(value);
        };
    }
}
