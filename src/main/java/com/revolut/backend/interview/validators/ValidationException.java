package com.revolut.backend.interview.validators;

public class ValidationException extends Exception {
    ValidationException(String message) {
        super(message);
    }
}
