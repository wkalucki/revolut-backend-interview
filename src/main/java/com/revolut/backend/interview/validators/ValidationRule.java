package com.revolut.backend.interview.validators;

import java.util.function.Predicate;

public class ValidationRule<T> implements Validation<T> {

    private final Predicate<T> rulePredicate;
    private final String ruleErrorMessage;

    ValidationRule(Predicate<T> rulePredicate, String ruleErrorMessage) {
        this.rulePredicate = rulePredicate;
        this.ruleErrorMessage = ruleErrorMessage;
    }

    @Override
    public ValidationResult test(T value) {
        return rulePredicate.test(value) ? ValidationResult.valid() : ValidationResult.invalid(ruleErrorMessage);
    }
}
