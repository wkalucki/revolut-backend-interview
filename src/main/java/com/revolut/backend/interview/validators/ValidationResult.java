package com.revolut.backend.interview.validators;

public class ValidationResult {

    private final boolean valid;
    private final String errorMessage;

    private ValidationResult(boolean valid, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    static ValidationResult valid() {
        return new ValidationResult(true, "");
    }

    static ValidationResult invalid(String errorMessage) {
        return new ValidationResult(false, errorMessage);

    }

    boolean isValid() {
        return valid;
    }

    void onInvalidThrow() throws ValidationException {
        if (!this.valid) throw new ValidationException(errorMessage);
    }

    public void onInvalidThrow(String ownMessage) throws ValidationException {
        if (!this.valid) throw new ValidationException(ownMessage);
    }
}
