package com.revolut.backend.interview.persistence;

import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.validators.TransactionValidator;
import com.revolut.backend.interview.validators.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class TransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);

    private final TransactionValidator transactionValidator;

    public TransactionService(TransactionValidator transactionValidator) {
        this.transactionValidator = transactionValidator;
    }

    public void transfer(Account fromCustomerAccount, Account toCustomerAccount, BigDecimal amount) throws ValidationException {
        transactionValidator.validateTransfer(fromCustomerAccount.getBalance(), toCustomerAccount.getBalance(), amount);
        commitTransfer(fromCustomerAccount, toCustomerAccount, amount);
    }

    private void commitTransfer(Account fromCustomerAccount, Account toCustomerAccount, BigDecimal amount) {
        LOGGER.info("Performing transfer from {} to {} for amount {}", fromCustomerAccount.getCustomerId(), toCustomerAccount.getCustomerId(), amount);
        commitOperation(fromCustomerAccount, toCustomerAccount, (accountFrom, accountTo) -> {
            accountFrom.setBalance(accountFrom.getBalance().subtract(amount));
            accountTo.setBalance(accountTo.getBalance().add(amount));

        });
    }

    private LockOrder<Account> getLockOrder(Account fromCustomerAccount, Account toCustomerAccount) {
        LockOrder<Account> lockOrder;
        if (fromCustomerAccount.getCustomerId().compareTo(toCustomerAccount.getCustomerId()) > 0) {
            lockOrder = new LockOrder<>(fromCustomerAccount, toCustomerAccount);
        } else {
            lockOrder = new LockOrder<>(toCustomerAccount, fromCustomerAccount);
        }
        return lockOrder;
    }

    private void commitOperation(Account account, Consumer<Account> operation) {
        account.lockAccount();
        operation.accept(account);
        account.unlockAccount();
    }

    private void commitOperation(Account accountFrom, Account accountTo, BiConsumer<Account, Account> operation) {
        LockOrder<Account> lockOrder = getLockOrder(accountFrom, accountTo);
        Account first = lockOrder.getFirst();
        Account second = lockOrder.getSecond();
        first.lockAccount();
        try {
            second.lockAccount();
            try {
                operation.accept(accountFrom, accountTo);
            } finally {
                second.unlockAccount();
            }
        } finally {
            first.unlockAccount();
        }
    }

    public void deposit(Account customerAccount, BigDecimal amount) throws ValidationException {
        LOGGER.info("Performing deposit for customer {} for amount {}", customerAccount.getCustomerId(), amount);
        transactionValidator.validateDeposit(amount);
        commitOperation(customerAccount, (account) -> {
            BigDecimal balance = account.getBalance();
            account.setBalance(balance.add(amount));
        });
    }
}
