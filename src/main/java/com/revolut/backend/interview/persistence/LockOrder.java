package com.revolut.backend.interview.persistence;

class LockOrder<T> {

    private final T first;
    private final T second;

    LockOrder(T first, T second) {
        this.first = first;
        this.second = second;
    }

    T getFirst() {
        return first;
    }

    T getSecond() {
        return second;
    }
}
