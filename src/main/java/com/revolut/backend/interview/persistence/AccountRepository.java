package com.revolut.backend.interview.persistence;

import com.revolut.backend.interview.model.Account;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class AccountRepository {

    private final Map<String, Account> accountPersistence;

    public AccountRepository(ConcurrentHashMap<String, Account> accountPersistence) {
        this.accountPersistence = accountPersistence;
    }

    public Account persist(Account account) throws DuplicateAccountException {
        Account prev = accountPersistence.putIfAbsent(account.getCustomerId(), account);
        if (prev != null) {
            throw new DuplicateAccountException();
        }
        return account;
    }

    public Optional<Account> findByCustomerId(String customerId) {
        return Optional.ofNullable(accountPersistence.get(customerId));
    }
}
