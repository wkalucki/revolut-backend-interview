package com.revolut.backend.interview.rest.handlers;

import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.model.Deposit;
import com.revolut.backend.interview.model.Transfer;
import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.persistence.TransactionService;
import com.revolut.backend.interview.validators.ValidationException;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

public class TransactionHandler {

    private final TransactionService transactionService;
    private final AccountRepository accountRepository;

    public TransactionHandler(AccountRepository accountRepository, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
    }

    public void performTransfer(@NotNull Context ctx) throws ValidationException {

        Transfer transfer = ctx.bodyAsClass(Transfer.class);
        Account fromCustomerAccount = getAccount(transfer.getFromCustomerId());
        Account toCustomerAccount = getAccount(transfer.getToCustomerId());
        transactionService.transfer(fromCustomerAccount, toCustomerAccount, transfer.getAmount());
    }

    private Account getAccount(String customerId) {
        return accountRepository.findByCustomerId(customerId).orElseThrow(IllegalArgumentException::new);
    }

    public void deposit(@NotNull Context context) throws ValidationException {
        Deposit deposit = context.bodyAsClass(Deposit.class);
        Account customerAccount = getAccount(deposit.getCustomerId());

        transactionService.deposit(customerAccount, deposit.getAmount());
    }
}
