package com.revolut.backend.interview.rest;

import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.persistence.TransactionService;
import com.revolut.backend.interview.rest.handlers.AccountHandler;
import com.revolut.backend.interview.rest.handlers.TransactionHandler;
import com.revolut.backend.interview.validators.TransactionValidator;
import com.revolut.backend.interview.validators.ValidationException;
import io.javalin.Javalin;
import io.javalin.core.util.RouteOverviewPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.javalin.apibuilder.ApiBuilder.*;

public class ApiServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiServer.class);
    private static final int BAD_REQUEST = 400;
    private static final int port = 8080;
    private final AccountRepository accountRepository;

    public ApiServer(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Javalin start() {
        LOGGER.info("Starting api server");
        Javalin app = Javalin.create(config -> config.registerPlugin(new RouteOverviewPlugin("/api")));

        AccountHandler accountHandler = new AccountHandler(accountRepository);
        TransactionHandler transactionHandler = new TransactionHandler(accountRepository, new TransactionService(new TransactionValidator()));

        app.routes(() -> {
                    path("api/account/:customerId", () -> {
                        post(accountHandler::createAccount);
                        get(accountHandler::retrieveAccount);
                    });
                    path("api/transfer", () -> {
                        post(transactionHandler::performTransfer);
                    });
                    path("api/deposit", () -> {
                        post(":customerId", transactionHandler::deposit);
                    });
                }
        );
        app.exception(ValidationException.class, ((exception, ctx) -> {
            ctx.status(BAD_REQUEST).result(exception.getMessage());
        }));
        app.start(port);

        return app;
    }
}
