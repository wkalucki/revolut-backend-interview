package com.revolut.backend.interview.rest.handlers;

import com.revolut.backend.interview.model.Account;
import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.persistence.DuplicateAccountException;
import com.revolut.backend.interview.validators.ValidationException;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static com.revolut.backend.interview.validators.ValidationHelper.notNullNonEmptyString;

public class AccountHandler {

    private static final String PROVIDED_CUSTOMER_ID_IS_INVALID = "Provided customerId is invalid";
    private static final String CUSTOMER_ID = "customerId";
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountHandler.class);
    private final AccountRepository accountRepository;

    public AccountHandler(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    public Account createAccount(@NotNull Context context) throws ValidationException {
        String customerId = context.pathParam(CUSTOMER_ID);
        validateCustomerId(customerId);
        Account account = new Account(customerId);
        LOGGER.info("Created account with id {}", account.getCustomerId());

        try {
            accountRepository.persist(account);
        } catch (DuplicateAccountException e) {
            throw new BadRequestResponse(e.getMessage());
        }
        return account;
    }

    public void retrieveAccount(@NotNull Context context) throws ValidationException {
        String customerId = context.pathParam(CUSTOMER_ID);
        validateCustomerId(customerId);
        Optional<Account> result = accountRepository.findByCustomerId(customerId);
        result.ifPresentOrElse(context::json, () -> {
            LOGGER.warn("Given customer does not exist {}", customerId);
            throw new NotFoundResponse(PROVIDED_CUSTOMER_ID_IS_INVALID);
        });
    }

    private void validateCustomerId(String customerId) throws ValidationException {
        notNullNonEmptyString.test(customerId).onInvalidThrow("Customer id is invalid");
    }
}
