package com.revolut.backend.interview;

import com.revolut.backend.interview.persistence.AccountRepository;
import com.revolut.backend.interview.rest.ApiServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        ApiServer apiServer = new ApiServer(new AccountRepository(new ConcurrentHashMap<>()));
        apiServer.start();
        logger.debug("Application started successfully");
    }
}
